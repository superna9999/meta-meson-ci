SUMMARY = "CI Testsuite"
LICENSE = "Apache-2.0"
LIC_FILES_CHKSUM = "file://${WORKDIR}/git/LICENSE;md5=3b83ef96387f14655fc854ddc3c6bd57"

inherit systemd

SRC_URI = "git://gitlab.com/amlogic-foss/ci-testsuite.git;protocol=https;branch=master"
SRCREV = "a9e344104280b0296ef20ebfa64e9c5f0ed4a972"

S = "${WORKDIR}/git"

SYSTEMD_PACKAGES = "${PN}"
SYSTEMD_SERVICE:${PN} = "ci-testsuite.service"

do_compile () {
}

do_install () {
	oe_runmake install SYSTEMD_PREFIX=${D}${systemd_system_unitdir} PREFIX=${D}${exec_prefix}
}

do_configure () {
}

FILES:${PN} += "/usr/lib/systemd/system-shutdown /usr/lib/ci-testsuite"
